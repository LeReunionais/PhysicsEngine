//
// Created by LeReunionais on 9/25/2016.
//

#ifndef GAMEPHYSICSENGINE_VECTOR3_H
#define GAMEPHYSICSENGINE_VECTOR3_H

#include <ostream>
#include "Precision.h"

namespace math {
    class OrthonormalBasis;

    class Vector3 {
    private:
        real x;
        real y;
        real z;
    public:
        Vector3(): x(0), y(0), z(0) {};
        Vector3(real x, real y, real z): x(x), y(y), z(z) {};
        ~Vector3() {};

        Vector3 operator+(const Vector3& a) const;
        Vector3 operator-(const Vector3& a) const;
        Vector3 operator*(const real a) const;
        real operator*(const Vector3& a) const;
        Vector3 operator%(const Vector3& a) const;

        Vector3 normalize() const;
        real magnitude() const;
        OrthonormalBasis makeOrthonormalBasis(const Vector3& a) const;

        bool operator==(Vector3 const&) const;
        bool operator!=(Vector3 const&) const;
        friend std::ostream &operator<<(std::ostream &os, const Vector3 &vector3);

        real getX() const;

        real getY() const;

        real getZ() const;
    };
}
#endif //GAMEPHYSICSENGINE_VECTOR3_H
