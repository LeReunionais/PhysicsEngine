//
// Created by LeReunionais on 9/25/2016.
//

#include <cmath>
#include "Vector3.h"
#include "OrthonormalBasis.h"

namespace math {
    Vector3 Vector3::operator*(const real a) const {
        return Vector3(
                this->x * a,
                this->y * a,
                this->z * a
        );
    }

    bool Vector3::operator==(const Vector3 &rhs) const {
        return x == rhs.x &&
               y == rhs.y &&
               z == rhs.z;
    }

    bool Vector3::operator!=(const Vector3 &rhs) const {
        return !(rhs == *this);
    }

    std::ostream &operator<<(std::ostream &os, const Vector3 &vector3) {
        os << "x: " << vector3.x << " y: " << vector3.y << " z: " << vector3.z;
        return os;
    }

    real Vector3::operator*(const Vector3 &a) const {
        return x * a.x + y * a.y + z * a.z;
    }

    Vector3 Vector3::operator%(const Vector3 &a) const {
        return Vector3(
                y * a.z - z * a.y,
                z * a.x - x * a.z,
                x * a.y - y * a.x
        );
    }

    Vector3 Vector3::operator+(const Vector3 &a) const {
        return Vector3(
                this->x + a.x,
                this->y + a.y,
                this->z + a.z
        );
    }

    Vector3 Vector3::operator-(const Vector3 &a) const {
        return Vector3(
                this->x - a.x,
                this->y - a.y,
                this->z - a.z
        );
    }

    Vector3 Vector3::normalize() const {
        real m = magnitude();
        if (m == 1) return *this;

        return *this * (1/m);
    }

    real Vector3::magnitude() const {
        return (real) sqrt(x * x + y * y + z * z);
    }

    OrthonormalBasis Vector3::makeOrthonormalBasis(const Vector3 &a) const {
        auto x = this->normalize();
        auto z = (x % a).normalize();
        auto y = z % x;
        return OrthonormalBasis(x, y, z);
    }

    real Vector3::getX() const {
        return x;
    }

    real Vector3::getY() const {
        return y;
    }


    real Vector3::getZ() const {
        return z;
    }
}
