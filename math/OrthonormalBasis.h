//
// Created by LeReunionais on 9/25/2016.
//

#ifndef GAMEPHYSICSENGINE_ORTHONORMALBASIS_H
#define GAMEPHYSICSENGINE_ORTHONORMALBASIS_H

#include <iostream>
#include "Vector3.h"
#include <stdexcept>

namespace math {
    class OrthonormalBasis {
    private:
        const Vector3 x;
        const Vector3 y;
        const Vector3 z;
    public:
        bool operator==(const OrthonormalBasis &rhs) const;

        bool operator!=(const OrthonormalBasis &rhs) const;

        friend std::ostream &operator<<(std::ostream &os, const OrthonormalBasis &basis);

        OrthonormalBasis(Vector3 x, Vector3 y, Vector3 z): x(x), y(y), z(z) {
            if (x.magnitude() != 1) {
                throw std::invalid_argument("x axis not normalized");
            }
            if (y.magnitude() != 1) {
                throw std::invalid_argument("y axis not normalized");
            }
            if (z.magnitude() != 1) {
                throw std::invalid_argument("z axis not normalized");
            }
            if (x * y != 0) {
                throw std::invalid_argument("x and y are not orthogonal");
            }
            if (y * z != 0) {
                throw std::invalid_argument("y and z are not orthogonal");
            }
            if (x * z != 0) {
                throw std::invalid_argument("x and y are not orthogonal");
            }
            if (x % y != z) {
                throw std::invalid_argument("x, y, z are not forming an orthogonal basis");

            }
        }
    };
}

#endif //GAMEPHYSICSENGINE_ORTHONORMALBASIS_H
