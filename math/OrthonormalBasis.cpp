//
// Created by LeReunionais on 9/25/2016.
//

#include <OrthonormalBasis.h>


namespace math {
    bool OrthonormalBasis::operator==(const OrthonormalBasis &rhs) const {
        return x == rhs.x &&
               y == rhs.y &&
               z == rhs.z;
    }

    bool OrthonormalBasis::operator!=(const OrthonormalBasis &rhs) const {
        return !(rhs == *this);
    }

    std::ostream &operator<<(std::ostream &os, const OrthonormalBasis &basis) {
        os << "x: " << basis.x << " y: " << basis.y << " z: " << basis.z;
        return os;
    }
}
