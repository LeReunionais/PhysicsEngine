#include <iostream>
#include <tclap/CmdLine.h>
#include <zmq/zmq.hpp>
#include <WorkRequestBuilder.h>
#include <job_generated.h>
#include <JobExecutor.h>

bool loop(zmq::socket_t *requestSocket, zmq::socket_t *pushSocket);

int main() {
    std::cout << "Game Physics Engine" << std::endl;

    std::string jobRequestEndpoint = "ipc://integrator_client/jobRequest";
    std::string workEndpoint = "ipc://integrator_client/work";

    zmq::context_t context(4);

    zmq::socket_t requestSocket(context, ZMQ_REQ);
    requestSocket.setsockopt(ZMQ_IMMEDIATE, 1);
    requestSocket.setsockopt(ZMQ_RCVTIMEO, 5000);
    requestSocket.connect(jobRequestEndpoint);
    std::cout << "REQ socket connected to " << jobRequestEndpoint << std::endl;

    zmq::socket_t pushSocket(context, ZMQ_PUSH);
    pushSocket.connect(workEndpoint);
    std::cout << "PUSH socket connected to " << workEndpoint << std::endl;

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wmissing-noreturn"
    int index = 0;
    while (true) {
        bool ok = loop(&requestSocket, &pushSocket);
        if (!ok) {
            requestSocket = zmq::socket_t(context, ZMQ_REQ);
            requestSocket.setsockopt(ZMQ_IMMEDIATE, 1);
            requestSocket.setsockopt(ZMQ_RCVTIMEO, 5000);
            requestSocket.connect(jobRequestEndpoint);
            std::cout << "REQ socket connected to " << jobRequestEndpoint << std::endl;
        } else {
            index++;
            std::cout << "JOB DONE: " << index << std::endl;
        }
    }
#pragma clang diagnostic pop
    return 0;
}

bool loop(zmq::socket_t *requestSocket, zmq::socket_t *pushSocket) {
    zmq::message_t requestMsg = zmq::message_t();

    auto requestBuilder = interface::WorkRequestBuilder();
    requestBuilder.fillRequest(&requestMsg);

    std::cout << "SENDING REQUEST FOR JOB" << std::endl;
    requestSocket->send(requestMsg.data(), requestMsg.size());
    std::cout << "REQUEST FOR JOB SENT" << std::endl;

    zmq::message_t replyMsg = zmq::message_t();
    std::cout << "WAITING FOR RESPONSE" << std::endl;
    bool ok = requestSocket->recv(&replyMsg);
    if (ok) {
        std::cout << "RESPONSE ARRIVED" << std::endl;
        const GamePhysicsEngine::JobResponse *jobResponse = GamePhysicsEngine::GetJobResponse(replyMsg.data());

        zmq::message_t pushMsg = zmq::message_t();
        auto executor = interface::JobExecutor();
        std::cout << "BUILDING RESPONSE" << std::endl;
        executor.execute(jobResponse, &pushMsg);
        std::cout << "RESPONSE BUILT" << std::endl;

        if (pushMsg.size() > 0) {
            std::cout << "SENDING WORK" << std::endl;
            pushSocket->send(pushMsg.data(), pushMsg.size());
            std::cout << "WORK SENT" << std::endl;
        } else {
            std::cout << "NO WORK" << std::endl;
        }
        return true;
    } else {
        std::cout << "TIMEOUT" << std::endl;
        return false;
    }

}