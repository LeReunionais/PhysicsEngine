#include <iostream>

//
// Created by LeReunionais on 10/21/2016.
//

#include <tclap/CmdLine.h>
#include <zmq/zmq.hpp>
#include <thread>
#include <Precision.h>
#include "../interface/job_generated.h"

void jobDistributor(zmq::socket_t *replySocket);
void work(zmq::socket_t *replySocket);

void buildArgv(std::vector<std::string> &argv);

void launchJob(zmq::socket_t *replySocket, math::real dampingState, math::real inverseMassState, math::real positionXState, math::real positionYState,
          math::real positionZState, math::real velocityXState, math::real velocityYState, math::real velocityZState,
          math::real accelerationXState, math::real accelerationYState, math::real accelerationZState, math::real deltaState);

void view(math::real dampingState, math::real inverseMassState, math::real positionXState, math::real positionYState,
          math::real positionZState, math::real velocityXState, math::real velocityYState, math::real velocityZState,
          math::real accelerationXState, math::real accelerationYState, math::real accelerationZState, math::real delta);

using namespace GamePhysicsEngine;

int main(int argc, const char** argv) {
    std::cout << "Employer" << std::endl;
    try{
        TCLAP::CmdLine cmd("Server that request job to integrator", ' ', "0");
        TCLAP::ValueArg<std::string> jobRequestArg(
                "j",
                "jobEndpoint",
                "Job request endpoint. Defaulted to ipc://jobRequest",
                false,
                "ipc://jobRequest",
                "string");
        cmd.add(jobRequestArg);
        TCLAP::ValueArg<std::string> workArg(
                "w",
                "workEndpoint",
                "Work endpoint. Defaulted to ipc://work",
                false,
                "ipc://work",
                "string");
        cmd.add(workArg);
        cmd.parse(argc, argv);

        zmq::context_t context(1);

        zmq::socket_t pullSocket(context, ZMQ_PULL);
        std::string workEndpoint = workArg.getValue();
        pullSocket.bind(workEndpoint);
        std::cout << "PULL bound to " << workEndpoint << std::endl;

        zmq::socket_t replySocket(context, ZMQ_REP);
        replySocket.bind(jobRequestArg.getValue());
        std::cout << "REPLY bound to " << jobRequestArg.getValue() << std::endl;

        std::thread tj(&jobDistributor, &replySocket);
        std::thread tw(&work, &pullSocket);

        tj.join();
    } catch (TCLAP::ArgException &e)
    { std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl;}
    return 0;
}

void work(zmq::socket_t *pullSocket) {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wmissing-noreturn"
    while (true) {
        zmq::message_t request;
        pullSocket->recv(&request);
        std::cout << "Received work result" << std::endl;
        auto work = flatbuffers::GetRoot<GamePhysicsEngine::IntegrationWork>(request.data());
        std::cout << "position x " << work->particle()->position()->x() << std::endl;
        std::cout << "position y " << work->particle()->position()->y() << std::endl;
        std::cout << "position z " << work->particle()->position()->z() << std::endl;
        std::cout << "velocity x " << work->particle()->velocity()->x() << std::endl;
        std::cout << "velocity y " << work->particle()->velocity()->y() << std::endl;
        std::cout << "velocity z " << work->particle()->velocity()->z() << std::endl;
    }
#pragma clang diagnostic pop
}

void jobDistributor(zmq::socket_t *replySocket) {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wmissing-noreturn"
    TCLAP::CmdLine cmd("Interactive prompt", ' ', "0");
    std::vector<std::string> allowedActions;
    allowedActions.push_back("launch");
    allowedActions.push_back("view");
    allowedActions.push_back("set");
    allowedActions.push_back("quit");

    TCLAP::ValuesConstraint<std::string> constraint(allowedActions);
    TCLAP::UnlabeledValueArg<std::string> action("action", "action to perform", true, "", &constraint);
    cmd.add(action);

    TCLAP::ValueArg<math::real> damping(
            "d",
            "damping",
            "Damping",
            false,
            1,
            "float");
    cmd.add(damping);

    TCLAP::ValueArg<math::real> inverseMass(
            "i",
            "inverseMass",
            "Inverse mass",
            false,
            1,
            "float");
    cmd.add(inverseMass);

    TCLAP::ValueArg<math::real> positionX(
            "x",
            "x",
            "Position x",
            false,
            0,
            "float");
    cmd.add(positionX);

    TCLAP::ValueArg<math::real> positionY(
            "y",
            "y",
            "Position y",
            false,
            0,
            "float");
    cmd.add(positionY);

    TCLAP::ValueArg<math::real> positionZ(
            "z",
            "z",
            "Position z",
            false,
            0,
            "float");
    cmd.add(positionZ);

    TCLAP::ValueArg<math::real> velocityX(
            "",
            "vx",
            "Velocity x",
            false,
            0,
            "float");
    cmd.add(velocityX);
    TCLAP::ValueArg<math::real> velocityY(
            "",
            "vy",
            "Velocity y",
            false,
            0,
            "float");
    cmd.add(velocityY);
    TCLAP::ValueArg<math::real> velocityZ(
            "",
            "vz",
            "Velocity z",
            false,
            0,
            "float");
    cmd.add(velocityZ);
    TCLAP::ValueArg<math::real> accelerationX(
            "",
            "ax",
            "Acceleration x",
            false,
            0,
            "float");
    cmd.add(accelerationX);
    TCLAP::ValueArg<math::real> accelerationY(
            "",
            "ay",
            "Acceleration y",
            false,
            0,
            "float");
    cmd.add(accelerationY);
    TCLAP::ValueArg<math::real> accelerationZ(
            "",
            "az",
            "Acceleration z",
            false,
            0,
            "float");
    cmd.add(accelerationZ);
    TCLAP::ValueArg<math::real> delta(
            "t",
            "delta",
            "Delta",
            false,
            0,
            "float");
    cmd.add(delta);

    std::string currentAction = "";
    math::real dampingState = damping.getValue();
    math::real inverseMassState = inverseMass.getValue();
    math::real positionXState = positionX.getValue();
    math::real positionYState = positionY.getValue();
    math::real positionZState = positionZ.getValue();
    math::real velocityXState = velocityX.getValue();
    math::real velocityYState = velocityY.getValue();
    math::real velocityZState = velocityZ.getValue();
    math::real accelerationXState = accelerationX.getValue();
    math::real accelerationYState = accelerationY.getValue();
    math::real accelerationZState = accelerationZ.getValue();
    math::real deltaState = delta.getValue();
    while(currentAction != "quit") {
        std::vector<std::string> argv;
        buildArgv(argv);
        try {
            cmd.reset();
            cmd.parse(argv);
            currentAction = action.getValue();
            std::cout << "Action: " << currentAction << std::endl;
            if (currentAction == "quit") {
                std::cout << "Goodbye" << std::endl;
            } else if (currentAction == "launch"){
                launchJob(replySocket, dampingState, inverseMassState, positionXState, positionYState, positionZState, velocityXState,
                     velocityYState, velocityZState, accelerationXState, accelerationYState, accelerationZState, deltaState);
            } else if (currentAction == "view"){
                view(dampingState, inverseMassState, positionXState, positionYState, positionZState, velocityXState,
                     velocityYState, velocityZState, accelerationXState, accelerationYState, accelerationZState, deltaState);
            } else if (currentAction == "set"){
                if (damping.isSet()) {
                    dampingState  = damping.getValue();
                }
                if (inverseMass.isSet()) {
                    inverseMassState  = inverseMass.getValue();
                }
                if (positionX.isSet()) {
                    positionXState  = positionX.getValue();
                }
                if (positionY.isSet()) {
                    positionYState  = positionY.getValue();
                }
                if (positionZ.isSet()) {
                    positionZState  = positionZ.getValue();
                }
                if (velocityX.isSet()) {
                    velocityXState  = velocityX.getValue();
                }
                if (velocityY.isSet()) {
                    velocityYState  = velocityY.getValue();
                }
                if (velocityZ.isSet()) {
                    velocityZState  = velocityZ.getValue();
                }
                if (accelerationX.isSet()) {
                    accelerationXState  = accelerationX.getValue();
                }
                if (accelerationY.isSet()) {
                    accelerationYState  = accelerationY.getValue();
                }
                if (accelerationZ.isSet()) {
                    accelerationZState  = accelerationZ.getValue();
                }
                if (delta.isSet()) {
                    deltaState  = delta.getValue();
                }
                view(dampingState, inverseMassState, positionXState, positionYState, positionZState, velocityXState,
                     velocityYState, velocityZState, accelerationXState, accelerationYState, accelerationZState, deltaState);
            } else {
            }
        } catch (TCLAP::ArgException &e) {
            std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl;
        } catch (TCLAP::ExitException &e) {
            std::cerr << "youpi" << std::endl;
        }
    }
#pragma clang diagnostic pop
}

void view(math::real dampingState, math::real inverseMassState, math::real positionXState, math::real positionYState,
          math::real positionZState, math::real velocityXState, math::real velocityYState, math::real velocityZState,
          math::real accelerationXState, math::real accelerationYState, math::real accelerationZState, math::real deltaState) {
    std::cout << "damping: " << dampingState << std::endl;
    std::cout << "inverseMass: " << inverseMassState << std::endl;
    std::cout << "position x: " << positionXState << std::endl;
    std::cout << "position y: " << positionYState << std::endl;
    std::cout << "position z: " << positionZState << std::endl;
    std::cout << "velocity x: " << velocityXState << std::endl;
    std::cout << "velocity y: " << velocityYState << std::endl;
    std::cout << "velocity z: " << velocityZState << std::endl;
    std::cout << "acceleration x: " << accelerationXState << std::endl;
    std::cout << "acceleration y: " << accelerationYState << std::endl;
    std::cout << "acceleration z: " << accelerationZState << std::endl;
    std::cout << "delta: " << deltaState << std::endl;
}

void launchJob(zmq::socket_t *replySocket, math::real dampingState, math::real inverseMassState, math::real positionXState, math::real positionYState,
          math::real positionZState, math::real velocityXState, math::real velocityYState, math::real velocityZState,
          math::real accelerationXState, math::real accelerationYState, math::real accelerationZState, math::real deltaState) {

    std::cout << "1 Jobs available" << std::endl;
    zmq::message_t request;
    replySocket->recv(&request);
    std::cout << "1 Worker ready" << std::endl;

    flatbuffers::FlatBufferBuilder builder;
    builder.Clear();

    auto position = Vector3(positionXState, positionYState, positionZState);
    auto velocity = Vector3(velocityXState, velocityYState, velocityZState);
    std::vector<Vector3> v = {Vector3(accelerationXState, accelerationYState, accelerationZState)};
    auto forces = builder.CreateVectorOfStructs(v);

    auto particle = CreateParticle(
            builder, &position, &velocity, forces, inverseMassState, dampingState
    );
    auto jobId = builder.CreateString("job");
    auto integrationJob = CreateIntegrationJob(builder, particle, deltaState, jobId);
    auto jobResponse = CreateJobResponse(builder, Job_IntegrationJob, integrationJob.Union());
    builder.Finish(jobResponse);

    uint8_t *buf = builder.GetBufferPointer();
    int size = builder.GetSize();
    zmq::message_t reply((size_t) size);
    memcpy(reply.data(), buf, (size_t) size);
    replySocket->send(reply);
    std::cout << "Sent job to worker" << std::endl;
}

void buildArgv(std::vector<std::string> &argv) {
    std::string args;
    std::cout << ">";
    getline(std::cin, args);
    std::stringstream ss(args);
    std::string item;
    argv.push_back("Prompt");
    while (getline(ss, item, ' ')) {
        argv.push_back(item);
    }
}

