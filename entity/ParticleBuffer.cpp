//
// Created by LeReunionais on 10/19/2016.
//

#include <particle_generated.h>
#include "ParticleBuffer.h"

const real entity::ParticleBuffer::getInverseMass() const {
    return buffer->inverseMass();
}

const real entity::ParticleBuffer::getDamping() const {
    return buffer->damping();
}

const Vector3 entity::ParticleBuffer::getPosition() const {
    return buffToVector3(buffer->position());
}

const Vector3 entity::ParticleBuffer::buffToVector3(const GamePhysicsEngine::Vector3 *buff) {
    return math::Vector3(
            buff->x(),
            buff->y(),
            buff->z()
    );
}

const Vector3 entity::ParticleBuffer::getVelocity() const {
    return buffToVector3(buffer->velocity());
}

const std::vector<Vector3> entity::ParticleBuffer::getForces() const {
    std::vector<Vector3> forces = {};
    for (int i = 0; i < buffer->forces()->size(); ++i) {
        forces.push_back(buffToVector3(buffer->forces()->Get((flatbuffers::uoffset_t) i)));
    }
    return forces;
}

entity::ParticleBuffer::ParticleBuffer(const GamePhysicsEngine::Particle *buffer) : buffer(buffer) {}
