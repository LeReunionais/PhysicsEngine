//
// Created by LeReunionais on 10/19/2016.
//

#ifndef GAMEPHYSICSENGINE_INTEGRATOR_H
#define GAMEPHYSICSENGINE_INTEGRATOR_H


#include "ParticleBase.h"

namespace entity {
    class Integrator {
    public:
        static const ParticleBase *integrate(const ParticleBase *p, const seconds delta);
    private:
        Integrator() {};
    };
}


#endif //GAMEPHYSICSENGINE_INTEGRATOR_H
