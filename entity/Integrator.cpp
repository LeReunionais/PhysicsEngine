//
// Created by LeReunionais on 10/19/2016.
//

#include <iostream>
#include "Integrator.h"
#include "Particle.h"

const entity::ParticleBase *entity::Integrator::integrate(const entity::ParticleBase *p, const seconds delta) {
    std::cout << "Start integration" << std::endl;
    const Vector3 &newPosition = p->getPosition() + p->getVelocity() * delta;
    std::cout << "Position computed" << std::endl;
    const Vector3 &newVelocity = (p->getVelocity() + p->getAcceleration() * delta) * powf(p->getDamping(), delta);
    std::cout << "Velocity computed" << std::endl;
    const Particle *particle = new Particle(
            p->getInverseMass(),
            p->getDamping(),
            newPosition,
            newVelocity,
            {}
    );
    std::cout << "Particle integrated" << std::endl;
    return particle;
}
