//
// Created by LeReunionais on 10/19/2016.
//

#ifndef GAMEPHYSICSENGINE_PARTICULEBUFFER_H
#define GAMEPHYSICSENGINE_PARTICULEBUFFER_H


#include <flatbuffers/flatbuffers.h>
#include "ParticleBase.h"

namespace entity {
    class ParticleBuffer : public ParticleBase {
    private:
        const GamePhysicsEngine::Particle *buffer;
        const static math::Vector3 buffToVector3(const GamePhysicsEngine::Vector3 *buff);
    public:
        ParticleBuffer(const GamePhysicsEngine::Particle *buffer);

        virtual const real getInverseMass() const override;

        virtual const real getDamping() const override;

        virtual const math::Vector3 getPosition() const override;

        virtual const math::Vector3 getVelocity() const override;

        virtual const std::vector<math::Vector3> getForces() const override;
    };
}


#endif //GAMEPHYSICSENGINE_PARTICULEBUFFER_H
