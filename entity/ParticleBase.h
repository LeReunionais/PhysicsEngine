//
// Created by LeReunionais on 10/8/2016.
//

#ifndef GAMEPHYSICSENGINE_PARTICLEBASE_H
#define GAMEPHYSICSENGINE_PARTICLEBASE_H

#include <ostream>
#include <Precision.h>
#include <Vector3.h>
#include <math.h>
#include <numeric>
#include "Unit.h"
#include <vector>

using namespace math;

namespace entity {
    class ParticleBase {
    public:
        virtual const math::real getInverseMass() const = 0;
        virtual const math::real getDamping() const = 0;
        virtual const math::Vector3 getPosition() const = 0;
        virtual const math::Vector3 getVelocity() const = 0;
        virtual const std::vector<math::Vector3> getForces() const = 0;

        const math::Vector3 getAcceleration() const {
            std::vector<math::Vector3> forces = getForces();
            return std::accumulate(forces.begin(), forces.end(), math::Vector3()) * getInverseMass();
        };

        bool operator==(const ParticleBase &pb) const {
            return getInverseMass() == pb.getInverseMass()
                   && getDamping() == pb.getDamping()
                   && getPosition() == pb.getPosition()
                   && getVelocity() == pb.getVelocity()
                   && getAcceleration() == pb.getAcceleration()
                    ;
        }

        bool operator!=(const ParticleBase &pb) const {
            return !(pb == *this);
        }

        friend std::ostream &operator<<(std::ostream &os, const ParticleBase &base) {
            return os << "inverseMass: " << base.getInverseMass()
                      << " damping: " << base.getDamping()
                      << " position: " << base.getPosition()
                      << " velocity: " << base.getVelocity()
                      << " acceleration: " << base.getAcceleration();
        }

    protected:
        ParticleBase() {};
    };
}


#endif //GAMEPHYSICSENGINE_PARTICLEBASE_H
