//
// Created by LeReunionais on 9/25/2016.
//

#include <functional>
#include <numeric>
#include "Particle.h"

namespace entity {
    entity::Particle::Particle(const Vector3 &position, const Vector3 &velocity, const std::vector<Vector3> &forces) : position(
            position), velocity(velocity) {
        this->forces.insert(this->forces.end(), forces.begin(), forces.end());
    }

    bool Particle::operator==(const Particle &rhs) const {
        return inverseMass == rhs.inverseMass &&
               damping == rhs.damping &&
               position == rhs.position &&
               velocity == rhs.velocity &&
               getAcceleration() == rhs.getAcceleration();
    }

    bool Particle::operator!=(const Particle &rhs) const {
        return !(rhs == *this);
    }

    std::ostream &operator<<(std::ostream &os, const Particle &particle) {
        os << "inverseMass: " << particle.inverseMass << " damping: " << particle.damping << " position: "
           << particle.position << " velocity: " << particle.velocity << " acceleration: " << particle.getAcceleration();
        return os;
    }

    Particle::Particle(real inverseMass, real damping, const Vector3 &position, const Vector3 &velocity,
                       const std::vector<Vector3> &forces) : inverseMass(inverseMass), damping(damping),
                                                             position(position), velocity(velocity), forces(forces) {}

    const real Particle::getInverseMass() const {
        return inverseMass;
    }

    const real Particle::getDamping() const {
        return damping;
    }

    const Vector3 Particle::getPosition() const {
        return position;
    }

    const Vector3 Particle::getVelocity() const {
        return velocity;
    }

    const Particle *Particle::integrate(seconds i) const {
        const Vector3 &newPosition = getPosition() + getVelocity() * i;
        const Vector3 &newVelocity = (getVelocity() + getAcceleration() * i) * powf(getDamping(), i);
        const Particle *particle = new Particle(
                getInverseMass(),
                getDamping(),
                newPosition,
                newVelocity,
                {}
        );
        return particle;
    }

    const std::vector<Vector3> Particle::getForces() const {
        return forces;
    }
}
