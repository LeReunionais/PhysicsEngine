//
// Created by LeReunionais on 9/25/2016.
//

#ifndef GAMEPHYSICSENGINE_PARTICLE_H
#define GAMEPHYSICSENGINE_PARTICLE_H

#include <ostream>
#include <vector>
#include <Vector3.h>
#include "Unit.h"
#include "ParticleBase.h"


namespace entity {
    class Particle : public ParticleBase {
    private:
        real inverseMass = 1;
        real damping = 1;
        math::Vector3 position;
        math::Vector3 velocity;
        std::vector<math::Vector3> forces;

    public:
        Particle(const math::Vector3 &position, const math::Vector3 &velocity, const std::vector<math::Vector3> &forces);

        Particle(real inverseMass, real damping, const math::Vector3 &position, const math::Vector3 &velocity,
                 const std::vector<math::Vector3> &forces);

        const Particle *integrate(const seconds i) const;

        bool operator==(const Particle &rhs) const;

        bool operator!=(const Particle &rhs) const;

        friend std::ostream &operator<<(std::ostream &os, const Particle &particle);

        const real getInverseMass() const;
        const real getDamping() const;
        const math::Vector3 getPosition() const;
        const math::Vector3 getVelocity() const;
        const std::vector<math::Vector3> getForces() const;
    };
}

#endif //GAMEPHYSICSENGINE_PARTICLE_H
