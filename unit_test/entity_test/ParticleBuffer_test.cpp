//
// Created by LeReunionais on 9/28/2016.
//


#include <gtest/gtest.h>
#include <Particle.h>
#include <flatbuffers/flatbuffers.h>
#include <particle_generated.h>
#include <ParticleBuffer.h>

using namespace entity;

TEST(ParticleBufferTest, createFromBuffer) {
    flatbuffers::FlatBufferBuilder builder;
    builder.Clear();

    auto position = GamePhysicsEngine::Vector3(1, 1, 1);
    auto velocity = GamePhysicsEngine::Vector3(1, 2, 3);
    std::vector<GamePhysicsEngine::Vector3> v = {GamePhysicsEngine::Vector3(3, 0, 0), GamePhysicsEngine::Vector3(0, 3, 3)};
    auto forces = builder.CreateVectorOfStructs(v);

    int inverseMass = 1;
    int damping = 2;
    const auto particle = CreateParticle(
            builder
            , &position
            , &velocity
            , forces
            , inverseMass
            , damping
    );
    builder.Finish(particle);

    uint8_t *buf = builder.GetBufferPointer();
    auto entity = GamePhysicsEngine::GetParticle(buf);

    ParticleBuffer particleBuffer(entity);
    EXPECT_EQ(particleBuffer.getDamping(), damping);
    EXPECT_EQ(particleBuffer.getInverseMass(), inverseMass);
    EXPECT_EQ(particleBuffer.getPosition(), Vector3(1, 1, 1));
    EXPECT_EQ(particleBuffer.getVelocity(), Vector3(1, 2, 3));
    std::vector<Vector3> expectedForces = {
            Vector3(3, 0, 0)
            , Vector3(0, 3, 3)
    };
    EXPECT_EQ(particleBuffer.getForces(), expectedForces);
    EXPECT_EQ(particleBuffer.getAcceleration(), Vector3(3, 3, 3));
}
