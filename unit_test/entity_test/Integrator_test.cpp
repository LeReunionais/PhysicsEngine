//
// Created by LeReunionais on 10/19/2016.
//
#include <Particle.h>
#include <Integrator.h>
#include "gtest/gtest.h"

using namespace entity;

TEST(IntegratorTest, integrate_not_moving_object) {
    ParticleBase *toIntegrate = new Particle(
            Vector3(),
            Vector3(),
            {Vector3()}
    );

    const ParticleBase *integrated = Integrator::integrate(toIntegrate, seconds(1));

    ParticleBase *expected = new Particle(
            Vector3(),
            Vector3(),
            {}
    );
    EXPECT_EQ(*integrated, *expected);
}

TEST(IntegratorTest, integrate_moving_object) {
    ParticleBase *toIntegrate = new Particle(
            Vector3(),
            Vector3(1, 2, 3),
            {Vector3()}
    );

    const ParticleBase *integrated = Integrator::integrate(toIntegrate, seconds(10));

    ParticleBase *expected = new Particle(
            Vector3(10, 20, 30),
            Vector3(1, 2, 3),
            {Vector3()}
    );
    EXPECT_EQ(*integrated, *expected);
}

TEST(IntegratorTest, integrate_accelerating_object) {
    ParticleBase* toIntegrate = new Particle(
            Vector3(),
            Vector3(),
            {Vector3(1, 1, 1)}
    );

    const ParticleBase *integrated = Integrator::integrate(toIntegrate, seconds(10));

    ParticleBase* expected = new Particle(
            Vector3(),
            Vector3(10, 10, 10),
            {Vector3()}
    );

    EXPECT_EQ(*integrated, *expected);
}

TEST(IntegratorTest, integrate_with_custom_mass) {
    Particle* toIntegrate = new Particle(
            2.0,
            1,
            Vector3(),
            Vector3(),
            {Vector3(1, 1, 1)}
    );

    const ParticleBase *integrated = Integrator::integrate(toIntegrate, seconds(10));

    Particle *expected = new Particle(
            2.0,
            1,
            Vector3(),
            Vector3(20, 20, 20),
            {Vector3()}
    );
    EXPECT_EQ(*integrated, *expected);
}

TEST(IntegratorTest, integrate_with_custom_damping) {
    Particle* toIntegrate = new Particle(
            1.0,
            0.999,
            Vector3(),
            Vector3(),
            {Vector3(1, 1, 1)}
    );

    const ParticleBase *integrated = Integrator::integrate(toIntegrate, seconds(10));

    Particle *expected = new Particle(
            1.0,
            0.999,
            Vector3(),
            Vector3(9.90045, 9.90045, 9.90045),
            {Vector3()}
    );
    EXPECT_EQ(*integrated, *expected);
}

