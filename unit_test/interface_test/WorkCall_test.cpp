//
// Created by LeReunionais on 11/1/2016.
//

#include <gtest/gtest.h>
#include <job_generated.h>
#include <zmq/zmq.hpp>
#include <thread>

using namespace GamePhysicsEngine;

void publisher(std::string endpoint, int nb_publication) {
    // Publisher
    zmq::context_t context(1);
    zmq::socket_t publisher(context, ZMQ_PUB);
    publisher.bind(endpoint);

    for (int i = 0; i < nb_publication; ++i) {
        zmq::message_t workContent(5);
        memcpy(workContent.data(), "youpi", 5);
        publisher.send(workContent);
        sleep(1);
    }
}

TEST(WorkCall_test, Simple) {
    // Subscriber
    zmq::context_t context(1);
    zmq::socket_t subscriber(context, ZMQ_SUB);
    subscriber.setsockopt(ZMQ_SUBSCRIBE, "", 0);

    subscriber.connect("ipc://publisher.ipc");

    std::thread th(&publisher, "ipc://publisher.ipc", 4);

    zmq::message_t publication;
    std::cout << "TRYING TO RECEIVE" << std::endl;
    subscriber.recv(&publication);
    std::string rpl = std::string(static_cast<char*>(publication.data()), publication.size());
    std::cout << rpl << std::endl;
    th.join();
}
