//
// Created by LeReunionais on 10/8/2016.
//

#include <gtest/gtest.h>
#include <Converter.h>

using namespace interface;

//TEST(ConverterTest, serializeDefaultParticle) {
//    auto c = Converter();
//    entity::Particle p = entity::Particle(
//            Vector3(),
//            Vector3(),
//            {}
//    );
//    const GamePhysicsEngine::Particle *serialized = c.serialize(p);
//
//    EXPECT_EQ(1.0, serialized->damping());
//    EXPECT_EQ(1.0, serialized->inverseMass());
//    EXPECT_EQ(0.0, serialized->position()->x());
//    EXPECT_EQ(0.0, serialized->position()->y());
//    EXPECT_EQ(0.0, serialized->position()->z());
//    EXPECT_EQ(0.0, serialized->velocity()->x());
//    EXPECT_EQ(0.0, serialized->velocity()->y());
//    EXPECT_EQ(0.0, serialized->velocity()->z());
//    EXPECT_EQ(0.0, serialized->forces()->size());
//}
//
//TEST(ConverterTest, serializeCustomParticle) {
//    Converter c = Converter();
//    entity::Particle p = entity::Particle(
//            2,
//            2,
//            Vector3(1, 1, 1),
//            Vector3(2, 3, 4),
//            {Vector3(3,3,3)}
//    );
//    const GamePhysicsEngine::Particle *serialized = c.serialize(p);
//
//    EXPECT_EQ(2.0, serialized->damping());
//    EXPECT_EQ(2.0, serialized->inverseMass());
//    EXPECT_EQ(1.0, serialized->position()->x());
//    EXPECT_EQ(1.0, serialized->position()->y());
//    EXPECT_EQ(1.0, serialized->position()->z());
//    EXPECT_EQ(2.0, serialized->velocity()->x());
//    EXPECT_EQ(3.0, serialized->velocity()->y());
//    EXPECT_EQ(4.0, serialized->velocity()->z());
//    EXPECT_EQ(1.0, serialized->forces()->size());
//    EXPECT_EQ(3.0, serialized->forces()->Get(0)->x());
//    EXPECT_EQ(3.0, serialized->forces()->Get(0)->y());
//    EXPECT_EQ(3.0, serialized->forces()->Get(0)->z());
//}

TEST(ConverterTest, deserializeDefault) {
    flatbuffers::FlatBufferBuilder builder;
    builder.Clear();

    std::vector<GamePhysicsEngine::Vector3> v = {};
    auto forces = builder.CreateVectorOfStructs(v);

    GamePhysicsEngine::ParticleBuilder particleBuilder(builder);
    particleBuilder.add_damping(1.0);
    particleBuilder.add_inverseMass(1.0);

    auto position = GamePhysicsEngine::Vector3(0, 0, 0);
    particleBuilder.add_position(&position);

    auto velocity = GamePhysicsEngine::Vector3(0, 0, 0);
    particleBuilder.add_velocity(&velocity);

    particleBuilder.add_forces(forces);

    auto particle = particleBuilder.Finish();

    builder.Finish(particle);
    uint8_t *buf = builder.GetBufferPointer();
    auto entity = GamePhysicsEngine::GetParticle(buf);

    auto c = Converter();
    auto deserialized = c.deserialize(entity);

    EXPECT_EQ(1.0, deserialized->getDamping());
    EXPECT_EQ(1.0, deserialized->getInverseMass());
    EXPECT_EQ(Vector3(), deserialized->getPosition());
    EXPECT_EQ(Vector3(), deserialized->getVelocity());
    EXPECT_EQ(Vector3(), deserialized->getAcceleration());

}

TEST(ConverterTest, deserializeCustom) {
    flatbuffers::FlatBufferBuilder builder;
    builder.Clear();
    std::vector<GamePhysicsEngine::Vector3> v = {GamePhysicsEngine::Vector3(3, 0, 0), GamePhysicsEngine::Vector3(0, 3, 3)};
    auto forces = builder.CreateVectorOfStructs(v);

    GamePhysicsEngine::ParticleBuilder particleBuilder(builder);
    particleBuilder.add_damping(2.0);
    particleBuilder.add_inverseMass(3.0);

    auto position = GamePhysicsEngine::Vector3(1, 1, 1);
    particleBuilder.add_position(&position);

    auto velocity = GamePhysicsEngine::Vector3(2, 2, 2);
    particleBuilder.add_velocity(&velocity);

    particleBuilder.add_forces(forces);

    auto particle = particleBuilder.Finish();
    builder.Finish(particle);
    uint8_t *buf = builder.GetBufferPointer();
    auto entity = GamePhysicsEngine::GetParticle(buf);


    auto c = Converter();
    auto deserialized = c.deserialize(entity);

    EXPECT_EQ(2.0, deserialized->getDamping());
    EXPECT_EQ(3.0, deserialized->getInverseMass());
    EXPECT_EQ(Vector3(1, 1, 1), deserialized->getPosition());
    EXPECT_EQ(Vector3(2, 2, 2), deserialized->getVelocity());
    EXPECT_EQ(Vector3(9, 9, 9), deserialized->getAcceleration());
}
