//
// Created by LeReunionais on 11/13/2016.
//

#include <gtest/gtest.h>
#include <WorkRequestBuilder.h>
#include <job_generated.h>

using namespace interface;

TEST(WorkRequestBuilder, fillRequest) {
    auto reqBuilder = WorkRequestBuilder();
    zmq::message_t request;

    reqBuilder.fillRequest(&request);

    const uint8_t *buf = (const uint8_t *) request.data();
    auto verifier = flatbuffers::Verifier(buf, request.size());
    bool ok = verifier.VerifyBuffer<GamePhysicsEngine::JobRequest>(nullptr);
    EXPECT_EQ(ok, true);
}

TEST(WorkRequestBuilder, notFillingRequest) {
    zmq::message_t request;

    const uint8_t *buf = (const uint8_t *) request.data();
    auto verifier = flatbuffers::Verifier(buf, request.size());
    bool ok = verifier.VerifyBuffer<GamePhysicsEngine::JobRequest>(nullptr);
    EXPECT_EQ(ok, false);
}
