//
// Created by LeReunionais on 9/26/2016.
//

#include <include/gtest/gtest.h>
#include <OrthonormalBasis.h>

using namespace math;

void NullAxis() {
    OrthonormalBasis(Vector3(), Vector3(), Vector3());
}
TEST(OrthonormalBasis, NullAxisThrowsException) {
    ASSERT_THROW(NullAxis(), std::invalid_argument);
}

void NotNormalizedXAxis() {
    OrthonormalBasis(Vector3(2, 0, 0), Vector3(1, 0, 0), Vector3(0, 1, 0));
}
TEST(OrthonormalBasis, NotNormalizedXAxis) {
    ASSERT_THROW(NotNormalizedXAxis(), std::invalid_argument);
}

void NotOrthogonalAxis() {
    OrthonormalBasis(Vector3(1, 0, 0), Vector3(0, 0, 1), Vector3(0, 1, 0));
}
TEST(OrthonormalBasis, NotOrthogonalAxis) {
    ASSERT_THROW(NotOrthogonalAxis(), std::invalid_argument);
}

TEST(OrthonormalBasis, Equality) {
    auto a = OrthonormalBasis(Vector3(1, 0, 0), Vector3(0, 1, 0), Vector3(0, 0, 1));
    auto b = OrthonormalBasis(Vector3(1, 0, 0), Vector3(0, 1, 0), Vector3(0, 0, 1));
    ASSERT_EQ(a, b);
}


