//
// Created by LeReunionais on 9/25/2016.
//

#include <list>
#include "gtest/gtest.h"

#include <Vector3.h>
#include <OrthonormalBasis.h>

using namespace math;

TEST(Vector, multiply) {
    EXPECT_EQ(Vector3(0, 0, 0), Vector3(0, 0, 0) * real(1));
    EXPECT_EQ(Vector3(2, 0, 0), Vector3(1, 0, 0) * real(2));
    EXPECT_EQ(Vector3(0, 3, 0), Vector3(0, 1, 0) * real(3));
    EXPECT_EQ(Vector3(0, 0, 4), Vector3(0, 0, 1) * real(4));
}

TEST(Vector, scalar_product) {
    EXPECT_EQ(real(0), Vector3(0, 0, 0) * Vector3(0, 0, 0));
    EXPECT_EQ(real(0), Vector3(1, 0, 0) * Vector3(0, 0, 0));
    EXPECT_EQ(real(1), Vector3(1, 0, 0) * Vector3(1, 0, 0));
    EXPECT_EQ(real(2), Vector3(1, 1, 0) * Vector3(1, 1, 0));
    EXPECT_EQ(real(3), Vector3(1, 1, 1) * Vector3(1, 1, 1));
}

TEST(Vector, vectorial_product) {
    EXPECT_EQ(Vector3(0, 0, 0),  Vector3(0, 0, 0) % Vector3(0,  0, 0));
    EXPECT_EQ(Vector3(0, 0, 0),  Vector3(1, 0, 0) % Vector3(0,  0, 0));
    EXPECT_EQ(Vector3(0, 0, 1),  Vector3(1, 0, 0) % Vector3(0,  1, 0));
    EXPECT_EQ(Vector3(1, 0, 0),  Vector3(0, 1, 0) % Vector3(0,  0, 1));
    EXPECT_EQ(Vector3(0, 0, -1), Vector3(1, 0, 0) % Vector3(0, -1, 0));
}
TEST(Vector, sum) {
    EXPECT_EQ(Vector3(0, 0, 0), Vector3(0, 0, 0) + Vector3(0, 0, 0));
    EXPECT_EQ(Vector3(1, 1, 1), Vector3(0, 0, 0) + Vector3(1, 1, 1));
    EXPECT_EQ(Vector3(3, 5, 7), Vector3(1, 2, 3) + Vector3(2, 3, 4));
}

TEST(Vector, substraction) {
    EXPECT_EQ(Vector3(),        Vector3()        - Vector3());
    EXPECT_EQ(Vector3(),        Vector3(1, 1, 1) - Vector3(1, 1, 1));
    EXPECT_EQ(Vector3(1, 0, 0), Vector3(1, 1, 1) - Vector3(0, 1, 1));
}

TEST(Vector, operator_equal) {
    Vector3 a = Vector3();
    Vector3 b = Vector3();
    EXPECT_EQ(a,b);
}

TEST(Vector, operator_not_equal) {
    Vector3 a = Vector3();
    Vector3 b = Vector3(1, 2, 4);
    EXPECT_NE(a,b);
}

TEST(Vector, normalize) {
    EXPECT_EQ(Vector3(1, 0, 0), Vector3(2, 0, 0).normalize());
    EXPECT_EQ(Vector3(0, 1, 0), Vector3(0, 45, 0).normalize());
    EXPECT_EQ(Vector3(0, 0, 1), Vector3(0, 0, 1000).normalize());
}

TEST(Vector, magnitude) {
    EXPECT_EQ(0, Vector3().magnitude());
    EXPECT_EQ(1, Vector3(1, 0, 0).magnitude());
    EXPECT_EQ(1, Vector3(0, 1, 0).magnitude());
    EXPECT_EQ(1, Vector3(0, 0, 1).magnitude());
    EXPECT_EQ(5, Vector3(3, 4, 0).magnitude());
}

TEST(Vector, makeOrthonormalBasis) {
    EXPECT_EQ(Vector3(1, 0, 0).makeOrthonormalBasis(Vector3(0, 1, 0)), OrthonormalBasis(Vector3(1, 0, 0), Vector3(0, 1, 0), Vector3(0, 0, 1)));
    EXPECT_EQ(Vector3(1, 0, 0).makeOrthonormalBasis(Vector3(0, 0, 1)), OrthonormalBasis(Vector3(1, 0, 0), Vector3(0, 0, 1), Vector3(0, -1, 0)));
    EXPECT_EQ(Vector3(7, 0, 0).makeOrthonormalBasis(Vector3(0, 0, 100)), OrthonormalBasis(Vector3(1, 0, 0), Vector3(0, 0, 1), Vector3(0, -1, 0)));
    EXPECT_EQ(Vector3(7, 0, 0).makeOrthonormalBasis(Vector3(0, 0, -100)), OrthonormalBasis(Vector3(1, 0, 0), Vector3(0, 0, -1), Vector3(0, 1, 0)));
}
