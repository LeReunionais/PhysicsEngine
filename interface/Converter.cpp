//
// Created by LeReunionais on 10/8/2016.
//

#include "Converter.h"

const entity::ParticleBase *interface::Converter::deserialize(const GamePhysicsEngine::Particle* p) const {
    std::vector<math::Vector3> forces = {};
    for (int i = 0; i < p->forces()->size(); ++i) {
        auto force = p->forces()->Get(i);
        forces.push_back(Vector3(force->x(), force->y(), force->z()));
    }
    return new entity::Particle(
            p->inverseMass(),
            p->damping(),
            Vector3(
                    p->position()->x(),
                    p->position()->y(),
                    p->position()->z()
            ),
            Vector3(
                    p->velocity()->x(),
                    p->velocity()->y(),
                    p->velocity()->z()
            ),
            forces
    );
}

const flatbuffers::Offset<GamePhysicsEngine::Particle>
interface::Converter::getOffset(const entity::ParticleBase &p, flatbuffers::FlatBufferBuilder &builder) const {
    std::vector<GamePhysicsEngine::Vector3> v;
    for (auto force : p.getForces()) {
        v.push_back(GamePhysicsEngine::Vector3(force.getX(), force.getY(), force.getZ()));
    }
    auto forces = builder.CreateVectorOfStructs(v);

    GamePhysicsEngine::ParticleBuilder particleBuilder(builder);
    particleBuilder.add_damping(p.getDamping());
    particleBuilder.add_inverseMass(p.getInverseMass());

    math::Vector3 particlePosition = p.getPosition();
    auto position = GamePhysicsEngine::Vector3(particlePosition.getX(), particlePosition.getY(), particlePosition.getZ());
    particleBuilder.add_position(&position);

    math::Vector3 particleVelocity = p.getVelocity();
    auto velocity = GamePhysicsEngine::Vector3(particleVelocity.getX(), particleVelocity.getY(), particleVelocity.getZ());
    particleBuilder.add_velocity(&velocity);

    particleBuilder.add_forces(forces);

    auto particle = particleBuilder.Finish();
    builder.Finish(particle);
    return particle;
}
