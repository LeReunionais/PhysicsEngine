//
// Created by LeReunionais on 11/13/2016.
//

#include <zmq/zmq.hpp>
#include "JobExecutor.h"
#include "Converter.h"
#include <Integrator.h>
#include <ParticleBuffer.h>

using namespace entity;
using namespace GamePhysicsEngine;

void interface::JobExecutor::execute(const JobResponse *jobResponse, zmq::message_t *workMsg) {
    Job job = jobResponse->job_type();
    switch (job) {
        case Job_IntegrationJob: {
            auto integrationJob = static_cast<const IntegrationJob*>(jobResponse->job());
            ParticleBuffer particleToIntegrate(integrationJob->particle());
            seconds delta = (seconds) (integrationJob->delta());
            const ParticleBase *integrated = Integrator::integrate(&particleToIntegrate, delta);

            flatbuffers::FlatBufferBuilder builder;
            Converter converter;
            auto particleBuffer = converter.getOffset(*integrated, builder);
            auto jobIdBuff = builder.CreateString(integrationJob->jobId()->str());
            auto integrationWork = GamePhysicsEngine::CreateIntegrationWork(builder, jobIdBuff, particleBuffer);
            builder.Finish(integrationWork);

            uint8_t *buf = builder.GetBufferPointer();
            size_t size = builder.GetSize();

            workMsg->rebuild(size);
            memcpy(workMsg->data(), buf, size);
            break;
        }
        case Job_NoJob:
            zmq_sleep(2);
            workMsg->rebuild(0);
            break;
        case Job_NONE:
            workMsg->rebuild(0);
            break;
    }
}
