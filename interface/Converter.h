//
// Created by LeReunionais on 10/8/2016.
//

#ifndef GAMEPHYSICSENGINE_CONVERTER_H
#define GAMEPHYSICSENGINE_CONVERTER_H

#include "particle_generated.h"
#include <Particle.h>

namespace interface {

    class Converter {
    public:
        const flatbuffers::Offset<GamePhysicsEngine::Particle> getOffset(const entity::ParticleBase& p, flatbuffers::FlatBufferBuilder& builder) const;
        const entity::ParticleBase *deserialize(const GamePhysicsEngine::Particle* p) const;
    };
}

#endif //GAMEPHYSICSENGINE_CONVERTER_H
