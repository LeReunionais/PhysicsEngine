//
// Created by LeReunionais on 11/13/2016.
//

#ifndef GAMEPHYSICSENGINE_WORKREQUESTBUILDER_H
#define GAMEPHYSICSENGINE_WORKREQUESTBUILDER_H


#include <zmq/zmq.hpp>

namespace interface {
    class WorkRequestBuilder {
    public:
        void fillRequest(zmq::message_t *request) const;
    };
}


#endif //GAMEPHYSICSENGINE_WORKREQUESTBUILDER_H
