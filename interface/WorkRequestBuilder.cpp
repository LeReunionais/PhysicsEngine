//
// Created by LeReunionais on 11/13/2016.
//

#include <flatbuffers/flatbuffers.h>
#include "WorkRequestBuilder.h"
#include "job_generated.h"

using namespace interface;

void WorkRequestBuilder::fillRequest(zmq::message_t *request) const {
    flatbuffers::FlatBufferBuilder builder;

    auto comment = builder.CreateString("Request for job");
    auto jobRequest = GamePhysicsEngine::CreateJobRequest(builder, comment);
    builder.Finish(jobRequest);

    uint8_t *buf = builder.GetBufferPointer();
    size_t size = builder.GetSize();

    request->rebuild(size);
    memcpy(request->data(), buf, size);
}
