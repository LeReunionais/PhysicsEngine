//
// Created by LeReunionais on 11/13/2016.
//

#ifndef GAMEPHYSICSENGINE_JOBDISPATCHER_H
#define GAMEPHYSICSENGINE_JOBDISPATCHER_H

#include "job_generated.h"

using namespace GamePhysicsEngine;
namespace interface {
    class JobExecutor {
    public:
        void execute(const JobResponse *jobResponse, zmq::message_t *workMsg);
    };
}


#endif //GAMEPHYSICSENGINE_JOBDISPATCHER_H
